## EP3_2018/1 - Movie On
<br/>
### Utilização
<br/>
Para utilização é necessário a instalação do Ruby.<br/>

	Navegue até a pasta onde realizou o git clone do projeto
	rode o comando bundle install 
	rode o comando rails s para abrir o server
	Agora basta acessar o localhost:3000 em seu navegador
<br/>
### Identificação
<br/>
Exercício Prático apresentado na matéria de OO pelo professor Renato Sampaio e produzido em **Ruby On Rails**.<br/>
<br/>**Aluno:** Pedro Rodrigues Pereira - **matrícula:** 17/0062686<br/>
**Aluno:** Daniel de Sousa Oliveira Melo Veras - **matrícula:** 17/0008371</br>
